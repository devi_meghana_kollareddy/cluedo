import pygame
import sys
import random
from tkinter import *

dicevalue=0
a=[0,0,16,23,18,5]
b=[16,7,0,7,23,23]
room=[0,0,0,0,0,0]
final1=[]
final2=[]
final3=[]
final4=[]
final5=[]
final6=[]
c=[]
rough=[0,1,2,3,4,5,6]
guess1=[]
guess2=[]
guess3=[]
guess4=[]
guess5=[]
guess6=[]
guesstemp1=[]
guesstemp2=[]
guesstemp3=[]
guesstemp4=[]
guesstemp5=[]
guesstemp6=[]

player1list=[]
player2list=[]
player3list=[]
player4list=[]
player5list=[]
player6list=[]

y=[1,1,1,1,1,1]

n=0


# root=Tk()
# root.geometry("400x400")

# l1=Label(root,font=("Helvetica",250))
 
# def roll():

#     dice=['\u2680','\u2681','\u2682','\u2683','\u2684','\u2685']

#     global dicevalue
#     dicevalue=random.choice(dice)
    
#     l1.config(text=f'{dicevalue}')
#     l1.pack()
#     if dicevalue==dice[0]:
#         dicevalue=1
#     elif dicevalue==dice[1]:
#         dicevalue=2
#     elif dicevalue==dice[2]:
#         dicevalue=3
#     elif dicevalue==dice[3]:
#         dicevalue=4
#     elif dicevalue==dice[4]:
#         dicevalue=5
#     else:
#         dicevalue=6
     
# b1=Button(root,text="Roll the Dice!",foreground='blue',command=roll)
# b1.place(x=250,y=100)
# b1.pack()
 
# root.mainloop()


#******************************************
things = {
    #suspects
    "S1": "MUSTARD",
    "S2": "ORCHID",

    "S3": "GREEN",
    "S4": "PEACOCK",
    "S5": "SCARLET",
    "S6": "PLUM",

    #weapona
    "W1": "CANDLESTICK",
    "W2": "DAGGER",
    "W3": "LEAD PIPE",
    "W4": "REVOLVER",
    "W5": "ROPE",
    "W6": "WRENCH",

    #rooms
    "R1": "HALL",
    "R2": "LOUNGE",
    "R3": "DINING ROOM",
    "R4": "KITCHEN",
    "R5": "BALLROOM",
    "R6": "CONSERVATORY",
    "R7": "BILLIARD ROOM",
    "R8": "LIBRARY",
    "R9": "STUDY"
}
players = [[],[]]
all_cards = ["S1","S2","S3","S4","S5","S6", "W1","W2","W3","W4","W5","W6", "R1","R2","R3","R4","R5","R6","R7","R8","R9"]
groups = [["S1","S2","S3","S4","S5","S6"],["W1","W2","W3","W4","W5","W6"],["R1","R2","R3","R4","R5","R6","R7","R8","R9"]]
c=groups

core=(random.sample(groups[0],1)+ random.sample(groups[1],1)+random.sample(groups[2],1))
print(core)

temp1=list(set(all_cards)-set(core))
print(temp1)

no_of_players=int(input("enter no.of players:"))
rough[0]=no_of_players
n=no_of_players


for i in range(0,no_of_players):

    temp_player=input("enter player name:")
    players[0].append(temp_player)
    if len(temp1)%no_of_players == 0:
        x=len(temp1)//no_of_players 
        players[1].append(random.sample(temp1, x))
        temp1=list(set(temp1)-set(players[1][i]))
        no_of_players-=1
    elif len(temp1)%no_of_players !=0:
        x=len(temp1)//no_of_players +1
        players[1].append(random.sample(temp1, x))
        temp1=list(set(temp1)-set(players[1][i]))
        no_of_players-=1

print(players)

def new():
	no_of_players=rough[0]
	i=no_of_players

	if i==no_of_players and i>0:
		new1=[word for word in groups[0] if word not in players[1][0]]
		new2=[word for word in groups[1] if word not in players[1][0]]
		new3=[word for word in groups[2] if word not in players[1][0]]
		player1list.append(new1)
		player1list.append(new2)
		player1list.append(new3)
		print(player1list)
		i-=1
	if i==(no_of_players-1) and i>0:
		new1=[word for word in groups[0] if word not in players[1][1]]
		new2=[word for word in groups[1] if word not in players[1][1]]
		new3=[word for word in groups[2] if word not in players[1][1]]
		player2list.append(new1)
		player2list.append(new2)
		player2list.append(new3)
		
		print(player2list)
		i-=1
	
	if i==(no_of_players-2) and i>0:
		new1=[word for word in groups[0] if word not in players[1][2]]
		new2=[word for word in groups[1] if word not in players[1][2]]
		new3=[word for word in groups[2] if word not in players[1][2]]
		player3list.append(new1)
		player3list.append(new2)
		player3list.append(new3)
		print(player3list)
		i-=1
	
	if i==(no_of_players-3) and i>0:
		new1=[word for word in groups[0] if word not in players[1][3]]
		new2=[word for word in groups[1] if word not in players[1][3]]
		new3=[word for word in groups[2] if word not in players[1][3]]
		player4list.append(new1)
		player4list.append(new2)
		player4list.append(new3)
		print(player4list)
		i-=1
	
	if i==(no_of_players-4) and i>0:
		new1=[word for word in groups[0] if word not in players[1][4]]
		new2=[word for word in groups[1] if word not in players[1][4]]
		new3=[word for word in groups[2] if word not in players[1][4]]
		player5list.append(new1)
		player5list.append(new2)
		player5list.append(new3)
		print(player5list)
		i-=1
	
	if i==(no_of_players-5) and i>0:
		new1=[word for word in groups[0] if word not in players[1][5]]
		new2=[word for word in groups[1] if word not in players[1][5]]
		new3=[word for word in groups[2] if word not in players[1][5]]
		player6list.append(new1)
		player6list.append(new2)
		player6list.append(new3)
		print(player6list)
		i-=1


#**************************

pygame.init()

white,black,blue,yellow,red,green,pink,brown,violet,black,orange,gray,aqua = (255,255,255),(0,0,0),(0,0,255),(255,255,0),(255,0,0),(0,255,0),(255,0,255),(165,42,42),(127,0,255),(0,0,0),(255,165,0),(169,169,169),(0,255,255)

width=600
height=600
size = 25
boardLength = 24
screen = pygame.display.set_mode((width,height))
pygame.display.set_caption("Cluedo Game")

def main():

	run=True
	i=2

	while run:


		screen.fill((0, 0, 0))
		draw_grid()

		while i>1:

			player1(random.randint(1,6))
			player1(random.randint(1,6))
			player2(random.randint(1,6))
			player2(random.randint(1,6))
			player3(random.randint(1,6))
			player3(random.randint(1,6))
			player4(random.randint(1,6))
			player4(random.randint(1,6))
			player5(random.randint(1,6))
			player5(random.randint(1,6))
			player6(random.randint(1,6))
			player6(random.randint(1,6))

			no_of_players=n
		
			if no_of_players==1:
				k=1
				while k>0:
					new()
					player1guess()
				
			elif no_of_players==2:
				k=1
				while k>0:
					new()
					player1guess()
					player2guess()
			elif no_of_players==3:
				k=1
				while k>0:
					new()
					player1guess()
					player2guess()
					player3guess()

			elif no_of_players==4:
				k=1
				while k>0:
					new()
					player1guess()
					player2guess()
					player3guess()
					player4guess()

			elif no_of_players==5:
				k=1
				while k>0:
					new()
					player1guess()
					player2guess()
					player3guess()
					player4guess()
					player5guess()
			elif no_of_players==6:
				k=1
				while k>0:
					new()
					player1guess()
					player2guess()
					player3guess()
					player4guess()
					player5guess()
					player6guess()
		
			
			i-=1

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			
		

def draw_grid():


	for i in range(0,boardLength):
		for j in range(0,boardLength):
			
			if i in range(0,5) and j <=5 :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==5 and j in range (0,4):
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==5 and j==5:
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(8,11) and j in range(0,8) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==11 and j in range(0,7) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range (12,14) and j in range(0,8) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==14 and j in range (0,6) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==14 and j==7 :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range (19,24) and j in range(0,7) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==18 and j in range (0,6):
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(0,4) and j in range(18,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==4 and j==18 :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==4 and j in range (20,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(9,12) and j in range(18,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==7 and j in range(18,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==8 and j in range(19,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(15,18) and j in range(17,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==13 and j in range(17,20) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==13 and j in range(21,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==14 and j in range(18,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(21,24) and j in range(17,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==20 and j in range(18,24) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(20,24) and j in range(9,15) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==18 and j in range(9,15) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==19 and j in range(9,14) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==17 and j in range(9,11) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==17 and j in range(12,15) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(0,4) and j in range(8,16) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==4 and j in range(9,15) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==5 and j in range(8,16) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==6 and j==8 :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==6 and j in range(10,14) :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i==6 and j==15 :
				pygame.draw.rect(screen, blue,[size*j,size*i,size,size])
			elif i in range(9,16) and j in range(10,15) :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==0 and j==7:
				pygame.draw.rect(screen, yellow,[size*j,size*i,size,size])
			elif i==0 and j==16:
				pygame.draw.rect(screen, pink,[size*j,size*i,size,size])
			elif i==16 and j==0:
				pygame.draw.rect(screen,green,[size*j,size*i,size,size])
			elif i==23 and j==7:
				pygame.draw.rect(screen, violet,[size*j,size*i,size,size])
			elif i==18 and j==23:
				pygame.draw.rect(screen, aqua,[size*j,size*i,size,size])
			elif i==5 and j==23:
				pygame.draw.rect(screen, orange,[size*j,size*i,size,size])
			elif i==5 and j==4:
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==11 and j==7:
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==14 and j==6 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==18 and j==6 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==4 and j==19 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==14 and j==17 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==13 and j==20 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==8 and j==18 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==20 and j==17 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==17 and j in range(11,13) :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==19 and j==14 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==4 and j==8:
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==4 and j==15 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==6 and j==9:
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			elif i==6 and j==14 :
				pygame.draw.rect(screen, gray,[size*j,size*i,size,size])
			else:
				pygame.draw.rect(screen, white, [size*j,size*i,size,size])
			
	grid()

def grid():
	x = 0; y = 0

	for i in range(boardLength):
		x += size
		pygame.draw.line(screen, black, (x, 0), (x, width))

	for i in range(boardLength):
		y += size
		pygame.draw.line(screen, black, (0, y), (height, y))			
		
def player1(dicevalue):

	print(dicevalue)
	i=a[0]
	j=b[0]
		
	if (i+dicevalue)>4:
		temp1=dicevalue-(4-i)
		i+=(4-i)
		a[0]=i
		j-=temp1
		b[0]=j
		room[0]=1
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])
	else:	
		i+=dicevalue
		a[0]=i
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])

	pygame.display.update()

	pygame.draw.rect(screen, white, [size*j,size*i,size,size])
		
def player6(dicevalue):
	
	print(dicevalue)
	i1=a[1]
	j1=b[1]
	if (i1+dicevalue)>6:
		temp1=i1
		i1+=(6-temp1)
		a[1]=i1
		temp2=dicevalue-(6-temp1)
		if temp2>=4:
			i1-=(temp2-3)
			a[1]=i1
			j1-=3
			b[1]=j1
			room[1]=1
			pygame.draw.rect(screen, red, [size*j1,size*i1,size,size])
		else:
			j1-=temp2
			b[1]=j1
			room[1]=1
			pygame.draw.rect(screen, red, [size*j1,size*i1,size,size])
	else:
		i1+=dicevalue
		a[1]=i1
		pygame.draw.rect(screen, red, [size*j1,size*i1,size,size])
	pygame.display.update()
	pygame.draw.rect(screen, white, [size*j1,size*i1,size,size])
	
def player5(dicevalue):
	print(dicevalue)
	i=a[2]
	j=b[2]
	
	if (j+dicevalue)>6:
		temp1=dicevalue-(6-j)
		j+=(6-j)
		b[2]=j
		i-=temp1
		a[2]=i
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])
		room[2]=1
	else:	
		j+=dicevalue
		b[2]=j
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])

	pygame.display.update()

	pygame.draw.rect(screen, white, [size*j,size*i,size,size])

def player4(dicevalue):
	print(dicevalue)
	i=a[3]
	j=b[3]

	if ((23-i)+dicevalue)>5:
		temp1=dicevalue-(5-(23-i))
		i-=(5-(23-i))
		a[3]=i
		j-=temp1
		b[3]=j
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])
		room[3]=1
	else:	
		i-=dicevalue
		a[3]=i
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])

	pygame.display.update()

	pygame.draw.rect(screen, white, [size*j,size*i,size,size])

def player3(dicevalue):
	print(dicevalue)
	i=a[4]
	j=b[4]
	
	if ((23-j)+dicevalue)>6:
		temp1=dicevalue-(6-(23-j))
		j-=(6-(23-j))
		b[4]=j
		i+=temp1
		a[4]=i
		if i>23:
			i-=1
			j+=1
			a[4]=i
			b[4]=j
			room[4]=1
			pygame.draw.rect(screen, red, [size*j,size*i,size,size])
		else:
			room[4]=1
			pygame.draw.rect(screen, red, [size*j,size*i,size,size])
	else:	
		j-=(23-j)+dicevalue
		b[4]=j
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])

	pygame.display.update()

	pygame.draw.rect(screen, white, [size*j,size*i,size,size])
	
def player2(dicevalue):
	print(dicevalue)
	i=a[5]
	j=b[5]
		
	if ((23-j)+dicevalue)>4:
		temp1=dicevalue-(4-(23-j))
		j-=(4-(23-j))
		b[5]=j
		i-=temp1
		a[5]=i
		if i<0:
			i+=1
			j+=1
			room[5]=1
			pygame.draw.rect(screen, red, [size*j,size*i,size,size])	
		else:
			room[5]=1
			pygame.draw.rect(screen, red, [size*j,size*i,size,size])
	else:	
		j-=(23-j)+dicevalue
		b[5]=j
		pygame.draw.rect(screen, red, [size*j,size*i,size,size])

	pygame.display.update()

	pygame.draw.rect(screen, white, [size*j,size*i,size,size])

def player1guess():
	guesstemp1=[]
	if len(player1list[0])>=1:
		guesstemp1=guesstemp1+random.sample(player1list[0],1)
	else:
		guesstemp1=guesstemp1+[]
	if len(player1list[1])>=1:
		guesstemp1=guesstemp1+random.sample(player1list[1],1)
	else:
		guesstemp1=guesstemp1+[]
	if len(player1list[2])>=1:
		guesstemp1=guesstemp1+random.sample(player1list[2],1)
	else:
		guesstemp1=guesstemp1+[]
	# guesstemp1=(random.sample(player1list[0],1)+random.sample(player1list[1],1)+random.sample(player1list[2],1))
	no_of_players=n
	for i in range(1,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp1:
				guess1.append(word)
	print("guesstemp1:",guesstemp1)
	print("guess1:",list(set(guess1)))
	x=list(set(guesstemp1)-set(guess1))
	guess1.clear()
	final1.extend(x)
	print(len(final1))
	player1list.clear()
	players[1][0]=(players[1][0]+guesstemp1)
	guesstemp1.clear()
	if len(final1)==3:
		y[0]=0
		sys.exit("player 1 won")
	elif len(players[1][0])==18:
		y[0]=0
		sys.exit("player1 won")

def player2guess():
	guesstemp2=[]
	if len(player2list[0])>=1:
		guesstemp2=guesstemp2+random.sample(player2list[0],1)
	else:
		guesstemp2=guesstemp2+[]
	if len(player2list[1])>=1:
		guesstemp2=guesstemp2+random.sample(player2list[1],1)
	else:
		guesstemp2=guesstemp2+[]
	if len(player2list[2])>=1:
		guesstemp2=guesstemp2+random.sample(player2list[2],1)
	else:
		guesstemp2=guesstemp2+[]
	# guesstemp2=(random.sample(player2list[0],1)+random.sample(player2list[1],1)+random.sample(player2list[2],1))
	no_of_players=n
	for i in range(0,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp2:
				guess2.append(word)
	print("guesstemp2:",guesstemp2)
	print("guess2:",list(set(guess2)))
	x=list(set(guesstemp2)-set(guess2))
	guess2.clear()
	final2.extend(x)
	print(len(final2))
	player2list.clear()
	players[1][1]=(players[1][1]+guesstemp2)
	guesstemp2.clear()
	if len(final2)==3:
		y[1]=0
		sys.exit("player2 won")
	elif len(players[1][1])==18:
		y[1]=0
		sys.exit("player2 won")

	


def player3guess():
	guesstemp3=[]
	if len(player3list[0])>=1:
		guesstemp3=guesstemp3+random.sample(player3list[0],1)
	else:
		guesstemp3=guesstemp3+[]
	if len(player3list[1])>=1:
		guesstemp3=guesstemp3+random.sample(player3list[1],1)
	else:
		guesstemp3=guesstemp3+[]
	if len(player3list[2])>=1:
		guesstemp3=guesstemp3+random.sample(player3list[2],1)
	else:
		guesstemp3=guesstemp3+[]
	# guesstemp3=(random.sample(player3list[0],1)+random.sample(player3list[1],1)+random.sample(player3list[2],1))
	no_of_players=n
	for i in range(0,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp3:
				guess3.append(word)
	print("guesstemp3:",guesstemp3)
	print("guess3:",list(set(guess3)))
	x=list(set(guesstemp3)-set(guess3))
	guess3.clear()
	final3.extend(x)
	print(len(final3))
	player3list.clear()
	players[1][2]=(players[1][2]+guesstemp3)
	guesstemp3.clear()
	if len(final3)==3:
		y[2]=0
		sys.exit("player3 won")
	elif len(players[1][2])==18:
		y[2]=0
		sys.exit("player3 won")

	


def player4guess():
	guesstemp4=[]
	if len(player4list[0])>=1:
		guesstemp4=guesstemp4+random.sample(player4list[0],1)
	else:
		guesstemp4=guesstemp4+[]
	if len(player4list[1])>=1:
		guesstemp4=guesstemp4+random.sample(player4list[1],1)
	else:
		guesstemp4=guesstemp4+[]
	if len(player4list[2])>=1:
		guesstemp4=guesstemp4+random.sample(player4list[2],1)
	else:
		guesstemp4=guesstemp4+[]
	# guesstemp4=(random.sample(player4list[0],1)+random.sample(player4list[1],1)+random.sample(player4list[2],1))
	no_of_players=n
	for i in range(0,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp4:
				guess4.append(word)
	print("guesstemp4:",guesstemp4)
	print("guess4:",list(set(guess4)))
	x=list(set(guesstemp4)-set(guess4))
	guess4.clear()
	final4.extend(x)
	print(len(final4))
	player4list.clear()
	players[1][3]=(players[1][3]+guesstemp4)
	guesstemp4.clear()
	if len(final4)==3:
		y[3]=0
		sys.exit("player4 won")
	elif len(players[1][3])==18:
		y[4]=0
		sys.exit("player4 won")

	


def player5guess():
	guesstemp5=[]
	if len(player5list[0])>=1:
		guesstemp5=guesstemp5+random.sample(player5list[0],1)
	else:
		guesstemp5=guesstemp5+[]
	if len(player5list[1])>=1:
		guesstemp5=guesstemp5+random.sample(player5list[1],1)
	else:
		guesstemp5=guesstemp5+[]
	if len(player5list[2])>=1:
		guesstemp5=guesstemp5+random.sample(player5list[2],1)
	else:
		guesstemp5=guesstemp5+[]
	# guesstemp5=(random.sample(player5list[0],1)+random.sample(player5list[1],1)+random.sample(player5list[2],1))
	no_of_players=n
	for i in range(0,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp5:
				guess5.append(word)
	print("guesstemp5:",guesstemp5)
	print("guess5:",list(set(guess5)))
	x=list(set(guesstemp5)-set(guess5))
	guess5.clear()
	final5.extend(x)
	print(len(final5))
	player5list.clear()
	players[1][4]=(players[1][4]+guesstemp5)
	guesstemp5.clear()
	if len(final5)==3:
		y[4]=0
		sys.exit("player5 won")
	elif len(players[1][4])==18:
		y[4]=0
		sys.exit("player5 won")

	

def player6guess():
	guesstemp6=[]
	if len(player6list[0])>=1:
		guesstemp6=guesstemp6+random.sample(player6list[0],1)
	else:
		guesstemp6=guesstemp6+[]
	if len(player6list[1])>=1:
		guesstemp6=guesstemp6+random.sample(player6list[1],1)
	else:
		guesstemp6=guesstemp6+[]
	if len(player6list[2])>=1:
		guesstemp6=guesstemp6+random.sample(player6list[2],1)
	else:
		guesstemp6=guesstemp6+[]
	# guesstemp6=(random.sample(player6list[0],1)+random.sample(player6list[1],1)+random.sample(player6list[2],1))
	no_of_players=n
	for i in range(0,no_of_players):
		for word in list(players[1][i]):
			if word in guesstemp6:
				guess6.append(word)
	print("guesstemp6:",guesstemp6)
	print("guess6:",list(set(guess6)))
	x=list(set(guesstemp6)-set(guess6))
	guess6.clear()
	final6.extend(x)
	print(len(final6))
	player6list.clear()
	players[1][5]=(players[1][5]+guesstemp6)
	guesstemp6.clear()
	if len(final6)==3:
		y[5]=0
		sys.exit("player6 won")
	elif len(players[1][5])==18:
		y[5]=0
		sys.exit("player6 won")

main()
