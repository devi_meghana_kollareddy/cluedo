import random
things = {

#Suspects
    "S1": "MUSTARD",
    "S2": "ORCHID",
    "S3": "GREEN",
    "S4": "PEACOCK",
    "S5": "SCARLET",
    "S6": "PLUM",
    
    
#Weapons
    "W1": "CANDLESTICK",
    "W2": "DAGGER",
    "W3": "LEAD PIPE",
    "W4": "REVOLVER",
    "W5": "ROPE",
    "W6": "WRENCH",


#Rooms
    "R1": "HALL",
    "R2": "LOUNGE",
    "R3": "DINING ROOM",
    "R4": "KITCHEN",
    "R5": "BALLROOM",
    "R6": "CONSERVATORY",
    "R7": "BILLIARD ROOM",
    "R8": "LIBRARY",
    "R9": "STUDY"
}
players = [[],[]]
all_cards = ["S1","S2","S3","S4","S5","S6", "W1","W2","W3","W4","W5","W6", "R1","R2","R3","R4","R5","R6","R7","R8","R9"]
groups = [["S1","S2","S3","S4","S5","S6"],["W1","W2","W3","W4","W5","W6"],["R1","R2","R3","R4","R5","R6","R7","R8","R9"]]

sol_cards = (random.sample(groups[0],1) +      random.sample(groups[1],1) + random.sample(groups[2],1))
print(sol_cards)

temp1 = list(set(all_cards) - set(sol_cards))
print(temp1)

no_of_players = int(input("Enter no.of players : "))

for i in range(0 , no_of_players) :

    temp_player = input("Enter player name : ")
    players[0].append(temp_player)
    if len(temp1) % no_of_players == 0 :
        x = len(temp1) // no_of_players 
        players[1].append(random.sample(temp1, x))
        temp1 = list(set(temp1) - set(players[1][i]))
        no_of_players -= 1
    elif len(temp1) % no_of_players != 0 :
        x = len(temp1) // no_of_players +1
        players[1].append(random.sample(temp1, x))
        temp1 = list(set(temp1) - set(players[1][i]))
        no_of_players -= 1

print(players)
