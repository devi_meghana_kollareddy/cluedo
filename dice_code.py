from tkinter import *
import random

root=Tk()
root.geometry("400x400")

l1=Label(root,font=("Helvetica",250))
 
def roll():

    dice=['\u2680','\u2681','\u2682','\u2683','\u2684','\u2685']

    dicevalue=random.choice(dice)
    print(dicevalue)
    
    l1.config(text=f'{dicevalue}')
    l1.pack()
     
b1=Button(root,text="Roll the Dice!",foreground='blue',command=roll)
b1.place(x=250,y=100)
b1.pack()
 
root.mainloop()
